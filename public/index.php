<pre>
<?php
require '../vendor/autoload.php';

use App\Forms\CallBackForm;
use App\Forms\RegisterForm;
use App\Validators\RegisterValidate;

$form = new CallBackForm();

$form->setData([
    'email' => 'test',
]);

dump($form->submit(), $form->getErrors());

$form->setData([
    'email' => 'test@test.est',
    'name'  => 'test',
]);
dump($form->submit(), $form->getErrors());


$form1 = new RegisterForm();

$form1->setData([
    'email'    => 'test@test.tes',
    'name'     => 'test2',
    'password' => 'test',
]);
dump($form1->submit(), $form1->getErrors());

$form2 = new RegisterForm(
    new RegisterValidate()
);

$form2->setData([
    'name'     => 'test12',
    'email'    => 'test@gmail.com',
    'password' => '12345678',
]);


dump($form2->submit(), $form2->getErrors());