<?php


namespace App\Forms;

use App\Forms\Contracts\FormInterface;
use App\Forms\Exceptions\FormException;
use App\Validators\AbstractValidator as Validator;

/**
 * Class AbstractForm
 * @package App\Forms
 */
abstract class AbstractForm implements FormInterface
{
    /**
     * @var Validator
     */
    private Validator $validator;

    /**
     * @var string|null
     */
    protected ?string $validatorClass;

    /**
     * @var array
     */
    private array $errors = [];

    /**
     * @var array
     */
    private array $fields;

    /**
     * @var array
     */
    protected array $data = [];

    /**
     * AbstractForm constructor.
     *
     * @param  Validator|null  $validator
     */
    public function __construct(?Validator $validator = null)
    {
        if ($validator) {
            $this->validator = $validator;
        } elseif ($this->validatorClass) {
            $this->validator = new $this->validatorClass();
        } else {
            throw  new FormException('Not Found Validator class');
        }

        $this->fields = $this->defaultFields();
    }

    /**
     * @return bool| array
     */
    public function submit()
    {
        if (! $this->validate()) {
            return false;
        }

        return $this->data;
    }

    /**
     * @return bool
     */
    public function validate(): bool
    {
        $validate = $this->validator
            ->validate();

        if (! $validate) {
            $this->setError($this->validator->errors());
        }

        return $validate;
    }

    /**
     * @param  mixed  $messages
     */
    protected function setError($messages): void
    {
        $this->errors = $messages;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param  array  $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
        $this->validator->setData($data);
    }

    /**
     * @return array
     */
    public function render(): array
    {
        return $this->fields;
    }

    /**
     * @param  Validator  $validator
     */
    public function setValidator(Validator $validator): void
    {
        $this->validator = $validator;
    }
}
