<?php


namespace App\Forms;

use App\Validators\CallBackFormValidate;

final class CallBackForm extends AbstractForm
{
    /**
     * @inheritdoc
     */
    protected ?string $validatorClass = CallBackFormValidate::class;

    /**
     * @inheritDoc
     */
    public function defaultFields(): array
    {
        return [
            'name'  => [
                'label' => 'Name',
                'type'  => 'text',
            ],
            'email' => [
                'label' => 'E-mail',
                'type'  => 'email',
            ],
        ];
    }
}
