<?php


namespace App\Forms\Contracts;

/**
 * Interface FormInterface
 * @package App\Forms\Contracts
 */
interface FormInterface
{
    /**
     * @return bool|array
     */
    public function submit();

    /**
     * @return bool
     */
    public function validate(): bool;

    /**
     * @return array
     */
    public function getErrors(): array;

    /**
     * @return array
     */
    public function defaultFields(): array;
}
