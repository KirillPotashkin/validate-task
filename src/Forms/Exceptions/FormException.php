<?php


namespace App\Forms\Exceptions;

use RuntimeException;

class FormException extends RuntimeException
{
}
