<?php


namespace App\Forms;

use App\Validators\RegisterValidate;

final class RegisterForm extends AbstractForm
{

    /**
     * @inheritdoc
     */
    protected ?string $validatorClass = RegisterValidate::class;

    /**
     * @inheritDoc
     */
    public function defaultFields(): array
    {
        return [
            'name'     => [
                'label' => 'Name',
                'type'  => 'text',
            ],
            'email'    => [
                'label' => 'E-mail',
                'type'  => 'email',
            ],
            'password' => [
                'label' => 'Pasword',
                'type'  => 'password',
            ],
        ];
    }
}
