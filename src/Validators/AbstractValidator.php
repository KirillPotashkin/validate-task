<?php

declare(strict_types=1);


namespace App\Validators;

use App\Validators\Contracts\ValidatorInterface;
use App\Validators\Rules\AbstractRule;

/**
 * Class AbstractValidator
 * @package App\Validators
 */
abstract class AbstractValidator implements ValidatorInterface
{

    /**
     * @var array
     */
    private array $errors = [];

    /**
     * @var array<array-key, mixed>
     */
    protected array $data = [];

    /**
     * @var array
     */
    protected array $rules = [];

    /**
     * AbstractValidator constructor.
     */
    public function __construct()
    {
        $this->rules = $this->rules();
    }

    /**
     * @psalm-suppress MixedArrayAssignment
     *
     * @param  string  $key
     * @param  mixed  $messages
     */
    protected function addError(string $key, $messages = null): void
    {
        $this->errors[$key][] = $messages;
    }

    /**
     * @return array
     */
    public function errors(): array
    {
        return $this->errors;
    }

    /**
     * @psalm-suppress MixedArgument
     * @psalm-suppress MixedMethodCall
     * @psalm-suppress MixedAssignment
     * @return bool
     */
    public function validate(): bool
    {
        $this->clearError();

        foreach ($this->rules as $key => $rules) {
            foreach ($rules as $rule) {
                /* @var $rule AbstractRule */
                $validate = $rule
                    ->setName($key)
                    ->setValue($this->data[$key] ?? null);

                $this->checkErrorRule($validate);
            }
        }
        if (! empty($this->errors)) {
            return false;
        }

        return true;
    }

    /**
     * @return void
     */
    private function clearError(): void
    {
        $this->errors = [];
    }

    /**
     * @param  AbstractRule  $rule
     */
    private function checkErrorRule(AbstractRule $rule): void
    {
        if (! $rule->validate()) {
            $this->addError($rule->getName(), ...$rule->getError());
        }
    }

    /**
     * @param  array  $data
     *
     * @return self
     */
    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return array
     */
    public function getRules(): array
    {
        return $this->rules;
    }
}
