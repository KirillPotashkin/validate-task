<?php

declare(strict_types=1);

namespace App\Validators;

use App\Validators\Rules\EmailRule;
use App\Validators\Rules\RequiredRule;
use App\Validators\Rules\StringRule;

/**
 * Class SubmitFormValidate
 * @package App\Validators
 */
final class CallBackFormValidate extends AbstractValidator
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'  => [
                StringRule::create(),
                RequiredRule::create(),
            ],
            'email' => [
                RequiredRule::create(),
                EmailRule::create(),
            ],
        ];
    }
}
