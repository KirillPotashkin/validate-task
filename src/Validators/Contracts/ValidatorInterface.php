<?php


namespace App\Validators\Contracts;

/**
 * Interface ValidatorInterface
 * @package App\Validators\Contracts
 */
interface ValidatorInterface
{
    /**
     * @return bool
     */
    public function validate(): bool;

    /**
     * @return array
     */
    public function errors(): array;

    /**
     * @return array
     */
    public function rules(): array;
}
