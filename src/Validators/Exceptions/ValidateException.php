<?php


namespace App\Validators\Exceptions;

use RuntimeException;

class ValidateException extends RuntimeException
{
}
