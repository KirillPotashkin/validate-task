<?php

declare(strict_types=1);

namespace App\Validators;

use App\Validators\Rules\EmailRule;
use App\Validators\Rules\RequiredRule;
use App\Validators\Rules\StringRule;

/**
 * Class SubmitFormValidate
 * @package App\Validators
 */
final class RegisterValidate extends AbstractValidator
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'email'    => [
                RequiredRule::create(),
                EmailRule::create(),
            ],
            'name'     => [
                RequiredRule::create(),
                StringRule::create(),
            ],
            'password' => [
                RequiredRule::create(),
                StringRule::create(['min_len' => 6]),
            ],
        ];
    }
}
