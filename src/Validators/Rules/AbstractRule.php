<?php

declare(strict_types=1);

namespace App\Validators\Rules;

use App\Validators\Rules\Contracts\Rule;

/**
 * Class AbstractRule
 * @package App\Validators\Rules
 * @psalm-consistent-constructor
 */
abstract class AbstractRule implements Rule
{
    /**
     * @var string
     */
    protected string $name = 'default';

    /**
     * @var mixed|null
     */
    protected $value;

    /**
     * @var array
     */
    private array $errors = [];

    /**
     * @var array
     */
    protected array $options = [];

    /**
     * AbstractRule constructor.
     *
     * @param  array  $options
     */
    protected function __construct(array $options = [])
    {
        $this->options = $options;
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param  string  $name
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param  mixed  $messages
     */
    public function addError($messages): void
    {
        $this->errors[] = $messages;
    }

    /**
     * @return array
     */
    public function getError(): array
    {
        return $this->errors;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param  mixed|null  $value
     *
     * @return self
     */
    public function setValue($value = null): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @param  array  $options
     *
     * @return self
     */
    public static function create(array $options = []): self
    {
        return new static($options);
    }

    /**
     * @param  string  $key
     *
     * @return mixed|null
     */
    public function getOption(string $key)
    {
        return $this->options[$key] ?? null;
    }

    /**
     * @param  array  $options
     *
     * @return self
     */
    public function setOptions(array $options): self
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @param  string  $key
     * @param  mixed  $value
     *
     * @return self
     */
    public function setOption(string $key, $value): self
    {
        $this->options[$key] = $value;

        return $this;
    }
}
