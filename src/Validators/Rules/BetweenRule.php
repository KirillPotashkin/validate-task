<?php


namespace App\Validators\Rules;

use App\Validators\Rules\Exceptions\BetweenException;

final class BetweenRule extends AbstractRule
{
    protected string $name = 'between';

    protected function __construct(array $options = [])
    {
        if (!isset($options['min']) && !isset($options['max'])) {
            throw new BetweenException('Required min and max are missing');
        }


        if ($options['min'] >= $options['max']) {
            throw new BetweenException('The min value must be less than the max');
        }

        if (! is_numeric($options['min']) && ! is_numeric($options['max'])) {
            throw new BetweenException('The minimum and maximum value must be a numeric');
        }

        parent::__construct($options);
    }

    public function validate(): bool
    {
        if (! is_numeric($this->value)) {
            $this->addError("{$this->name} must be a number");

            return false;
        }

        if ($this->options['min'] >= $this->value) {
            $this->addError("{$this->name} must be greater than to the {$this->options['min']}");
        }

        if ($this->options['max'] <= $this->value) {
            $this->addError("The {$this->name} value must be less than to the {$this->options['max']} value");
        }

        if (! empty($this->getError())) {
            return false;
        }

        return true;
    }
}
