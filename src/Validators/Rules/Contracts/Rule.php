<?php


namespace App\Validators\Rules\Contracts;

/**
 * Interface Rule
 * @package App\Validators\Rules\Contracts
 */
interface Rule
{
    /**
     *
     *
     * @return bool
     */
    public function validate(): bool;

    /**
     * @return array
     */
    public function getError(): array;

    /**
     * @param  mixed  $messages
     */
    public function addError($messages): void;
}
