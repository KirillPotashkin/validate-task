<?php

declare(strict_types=1);


namespace App\Validators\Rules;

final class EmailRule extends AbstractRule
{
    protected string $name = 'email';

    public function validate(): bool
    {
        if (! filter_var($this->value, FILTER_VALIDATE_EMAIL)) {
            $this->addError("{$this->name} not valid");
        }

        if (! empty($this->getError())) {
            return false;
        }

        return true;
    }
}
