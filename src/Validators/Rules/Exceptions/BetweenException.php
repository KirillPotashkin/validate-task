<?php


namespace App\Validators\Rules\Exceptions;

use App\Validators\Exceptions\ValidateException;

class BetweenException extends ValidateException
{
}
