<?php

declare(strict_types=1);

namespace App\Validators\Rules;

final class NumericRule extends AbstractRule
{
    protected string $name = 'numeric';

    public function validate(): bool
    {
        if (! is_numeric($this->value)) {
            $this->addError("{$this->name} must be number");
        }

        if (! empty($this->getError())) {
            return false;
        }

        return true;
    }
}
