<?php


namespace App\Validators\Rules;

final class RequiredRule extends AbstractRule
{
    public function validate(): bool
    {
        if (empty($this->value)) {
            $this->addError("{$this->name} field is required");
        }

        if (! empty($this->getError())) {
            return false;
        }

        return true;
    }
}
