<?php


namespace App\Validators\Rules;

final class StringRule extends AbstractRule
{
    public function validate(): bool
    {
        if (! is_string($this->value)) {
            $this->addError("{$this->getName()} must be string");

            return false;
        }

        $minLen = $this->getOption('min_len') ?? 0;
        $maxLen = $this->getOption('max_len') ?? 255;


        if (strlen($this->value) <= $minLen) {
            $this->addError("{$this->getName()} should be minimum {$minLen} characters");
        }

        if (strlen($this->value) >= $maxLen) {
            $this->addError("{$this->getName()} should be maximum {$maxLen} characters");
        }

        if (! empty($this->getError())) {
            return false;
        }

        return true;
    }
}
