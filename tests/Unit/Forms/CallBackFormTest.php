<?php


namespace Forms;

use App\Forms\AbstractForm;
use App\Forms\CallBackForm;
use PHPUnit\Framework\TestCase;

class CallBackFormTest extends TestCase
{
    private AbstractForm $form;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->form = new CallBackForm();
    }

    public function testSubmitFormSuccess(): void
    {
        $this->form->setData([
            'name'  => 'test',
            'email' => 'test@gmail.com',
        ]);

        self::assertNotEmpty($this->form->submit());
    }

    public function testSubmitFail(): void
    {
        $this->form->setData([
            'email' => 'test@gmail.com',
        ]);

        self::assertFalse($this->form->submit());

        $this->form->setData([]);

        self::assertFalse($this->form->submit());

        $this->form->setData([
            'name' => 'test',
        ]);

        self::assertFalse($this->form->submit());
    }
}
