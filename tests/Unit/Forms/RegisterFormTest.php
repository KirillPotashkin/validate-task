<?php


namespace Forms;

use App\Forms\AbstractForm;
use App\Forms\RegisterForm;
use App\Validators\RegisterValidate;
use PHPUnit\Framework\TestCase;

class RegisterFormTest extends TestCase
{
    private AbstractForm $form;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->form = new RegisterForm(new RegisterValidate());
    }

    public function testSubmitFormSuccess(): void
    {
        $this->form->setData([
            'name'     => 'test',
            'email'    => 'test@gmail.com',
            'password' => 'testtest',
        ]);

        self::assertNotEmpty($this->form->submit());
    }

    public function testSubmitFailPassword(): void
    {
        $this->form->setData([
            'name'     => 'test',
            'email'    => 'test@gmail.com',
            'password' => 'test',
        ]);

        self::assertFalse($this->form->submit());
        self::assertArrayHasKey('password', $this->form->getErrors());
    }
}
