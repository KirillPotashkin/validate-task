<?php


namespace Test\Unit\Rules;

use App\Validators\Rules\BetweenRule;
use App\Validators\Rules\Exceptions\BetweenException;
use PHPUnit\Framework\TestCase;

class BetweenRuleTest extends TestCase
{


    /**
     * EmailRuleTest constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function testValid(): void
    {
        $betweenRule = BetweenRule::create(['min' => 1, 'max' => 50]);

        $betweenRule->setValue(12);

        self::assertTrue($betweenRule->validate());
        self::assertCount(0, $betweenRule->getError());
    }

    public function testNotValid(): void
    {
        $betweenRule = BetweenRule::create(['min' => 1, 'max' => 50]);
        $betweenRule->setValue(120);

        self::assertFalse($betweenRule->validate());
        self::assertNotEmpty($betweenRule->getError());
    }

    public function testOption(): void
    {
        $this->expectException(BetweenException::class);

        BetweenRule::create(['min' => 60, 'max' => 50]);
    }
}
