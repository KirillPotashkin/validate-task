<?php


namespace Test\Unit\Rules;

use App\Validators\Rules\AbstractRule;
use App\Validators\Rules\EmailRule;
use PHPUnit\Framework\TestCase;

class EmailRuleTest extends TestCase
{
    private AbstractRule $email;

    /**
     * EmailRuleTest constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->email = EmailRule::create();
    }

    public function testValidEmail(): void
    {
        $this->email->setValue('test@gmail.com');

        self::assertTrue($this->email->validate());
        self::assertCount(0, $this->email->getError());
    }

    public function testNotValidEmail(): void
    {
        $this->email->setValue('test');

        self::assertFalse($this->email->validate());
        self::assertNotEmpty($this->email->getError());
    }
}
