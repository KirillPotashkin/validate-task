<?php


namespace Test\Unit\Rules;

use App\Validators\Rules\AbstractRule;
use App\Validators\Rules\NumericRule;
use PHPUnit\Framework\TestCase;

class NumericRuleTest extends TestCase
{
    private AbstractRule $numeric;

    /**
     * numericRuleTest constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->numeric = NumericRule::create();
    }

    public function testValid(): void
    {
        $this->numeric->setValue(124);

        self::assertTrue($this->numeric->validate());
        self::assertCount(0, $this->numeric->getError());
    }

    public function testNotValid(): void
    {
        $this->numeric->setValue('test');

        self::assertFalse($this->numeric->validate());
        self::assertNotEmpty($this->numeric->getError());
    }
}
