<?php


namespace Test\Unit\Rules;

use App\Validators\Rules\AbstractRule;
use App\Validators\Rules\RequiredRule;
use PHPUnit\Framework\TestCase;

class RequiredRuleTest extends TestCase
{
    private AbstractRule $requiredRule;

    /**
     * requiredRuleRuleTest constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->requiredRule = RequiredRule::create();
    }

    public function testValid(): void
    {
        $this->requiredRule->setValue(124);

        self::assertTrue($this->requiredRule->validate());
        self::assertCount(0, $this->requiredRule->getError());
    }

    public function testNotValid(): void
    {
        $this->requiredRule->setValue();

        self::assertFalse($this->requiredRule->validate());
        self::assertNotEmpty($this->requiredRule->getError());
    }
}
