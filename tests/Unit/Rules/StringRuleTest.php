<?php


namespace Test\Unit\Rules;

use App\Validators\Rules\AbstractRule;
use App\Validators\Rules\StringRule;
use PHPUnit\Framework\TestCase;

class StringRuleTest extends TestCase
{
    private AbstractRule $string;

    /**
     * stringRuleTest constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->string = StringRule::create();
    }

    public function testValid(): void
    {
        $this->string->setValue("test21412");

        self::assertTrue($this->string->validate());
        self::assertCount(0, $this->string->getError());
    }

    public function testNotValid(): void
    {
        $this->string->setValue(21412412);

        self::assertFalse($this->string->validate());
        self::assertNotEmpty($this->string->getError());
    }

    public function testMinValueFail(): void
    {
        $this->string->setOption('min_len', 6);
        $this->string->setValue('test');

        self::assertFalse($this->string->validate());
        self::assertNotEmpty($this->string->getError());
    }

    public function testMinValueSuccess(): void
    {
        $this->string->setOption('min_len', 6);
        $this->string->setValue('test12122');

        self::assertTrue($this->string->validate());
        self::assertEmpty($this->string->getError());
    }
}
