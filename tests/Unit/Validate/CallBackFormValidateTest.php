<?php


namespace Test\Unit\Validate;

use App\Validators\AbstractValidator;
use App\Validators\CallBackFormValidate;
use PHPUnit\Framework\TestCase;

class CallBackFormValidateTest extends TestCase
{
    private AbstractValidator $validator;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->validator = new CallBackFormValidate();
        $this->validator->setData([
            'name'  => 'test',
            'email' => 'test@gmail.com',
        ]);
    }

    public function testValid(): void
    {
        $this->validator->setData([
            'name'  => 'test',
            'email' => 'test@gmail.com',
        ]);

        self::assertTrue($this->validator->validate());
        self::assertEmpty($this->validator->errors());
    }

    public function testNameIsString(): void
    {
        $this->validator->setData([
            'name' => 123124,
            'email' => 'test@gmail.com',
        ]);

        self::assertFalse($this->validator->validate());
        self::assertNotEmpty($this->validator->errors());
    }

    public function testNameReq(): void
    {
        $this->validator->setData([
            'name' => '',
            'email' => 'test@gmail.com',
        ]);

        self::assertFalse($this->validator->validate());

        $this->validator->setData([
            'name' => null,
            'email' => 'test@gmail.com',
        ]);

        self::assertFalse($this->validator->validate());

        $this->validator->setData([
            'email' => 'test@gmail.com',
        ]);

        self::assertFalse($this->validator->validate());
    }

    public function testEmailReq(): void
    {
        $this->validator->setData([
            'name'  => 'test',
            'email' => '',
        ]);

        self::assertFalse($this->validator->validate());

        $this->validator->setData([
            'name'  => 'test',
            'email' => null,
        ]);

        self::assertFalse($this->validator->validate());

        $this->validator->setData([
            'name'  => 'test',
        ]);

        self::assertFalse($this->validator->validate());
    }
}
