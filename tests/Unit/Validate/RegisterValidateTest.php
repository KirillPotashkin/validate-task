<?php


namespace Test\Unit\Validate;

use App\Validators\AbstractValidator;
use App\Validators\RegisterValidate;
use PHPUnit\Framework\TestCase;

class RegisterValidateTest extends TestCase
{
    private AbstractValidator $validator;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->validator = new RegisterValidate();
    }

    public function testValid(): void
    {
        $this->validator->setData([
            'email'    => 'test@test.test',
            'name'     => 'test',
            'password' => '1234567',
        ]);

        self::assertTrue($this->validator->validate());
        self::assertEmpty($this->validator->errors());
    }

    public function testPasswordFail(): void
    {
        $this->validator->setData([
            'email'    => 'test@test.test',
            'name'     => 'test',
            'password' => '12345',
        ]);

        self::assertFalse($this->validator->validate());
        self::assertNotEmpty($this->validator->errors());
        self::assertArrayHasKey('password', $this->validator->errors());
    }

    public function testEmailFail(): void
    {
        $this->validator->setData([
            'name'     => 'test',
            'password' => '12345',
        ]);

        self::assertFalse($this->validator->validate());
        self::assertNotEmpty($this->validator->errors());
        self::assertArrayHasKey('email', $this->validator->errors());
    }

    public function tesNameFail(): void
    {
        $this->validator->setData([
            'email'    => 'test@test.test',
            'password' => '12345',
        ]);

        self::assertFalse($this->validator->validate());
        self::assertNotEmpty($this->validator->errors());
        self::assertArrayHasKey('name', $this->validator->errors());
    }
}
